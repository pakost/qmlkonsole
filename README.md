# QMLKonsole <img src="logo.png" width="40"/> 

A terminal built for mobile devices, based on [qmltermwidget](https://github.com/Swordfish90/qmltermwidget).

## Links

* Project page: https://invent.kde.org/plasma-mobile/qmlkonsole
* File issues: https://invent.kde.org/plasma-mobile/qmlkonsole/-/issues
* Development channel: https://matrix.to/#/#plasmamobile:matrix.org

## Building

```
mkdir build
cd build
cmake ..
make
```
